window.addEventListener("DOMContentLoaded", function () {
    getStatistics()
    document.getElementById("filterText").addEventListener("change", function (event) {
        filter(document.getElementById("filterSelect").value, event.target.value)
    })
    document.getElementById("filterSelect").addEventListener("change", function (event) {
        filter(event.target.value, document.getElementById("filterText").value)
    })
});

var jsonData = []
function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;

    return [year, month, day].join('-');
}
function getStatistics() {

    fetch('/api/dashboard/statistics', {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(async value => {
        jsonData = await value.json()
        displayData(jsonData)
    });

}


function displayData(val) {
    document.getElementById("tableData").innerHTML = ""
    val.forEach(element => {
        var row = document.createElement("tr")
        var name = document.createElement("td")
        name.textContent = element.name
        row.appendChild(name)
        var city = document.createElement("td")
        city.textContent = element.city
        row.appendChild(city)
        var spot = document.createElement("td")
        spot.textContent = element.spot
        row.appendChild(spot)
        var date = document.createElement("td")
        date.textContent = formatDate(element.date)
        row.appendChild(date)
        var hourstart = document.createElement("td")
        var spiltTimeStart = element.start.split(":")
        if (spiltTimeStart.length >= 2)
            hourstart.textContent = spiltTimeStart[0] + ":" + spiltTimeStart[1]
        else
            hourstart.textContent = ""
        row.appendChild(hourstart)
        var hourend = document.createElement("td")
        var spiltTimeEnd = element.end.split(":")
        if (spiltTimeEnd.length >= 2)
            hourend.textContent = spiltTimeEnd[0] + ":" + spiltTimeEnd[1]
        else
            hourend.textContent = ""
        row.appendChild(hourend)
        var duration = document.createElement("td")
        var spiltTimeDuration = element.duration.split(":")
        if (spiltTimeDuration.length >= 2)
            duration.textContent = spiltTimeDuration[0] + ":" + spiltTimeDuration[1]
        else
            duration.textContent = ""
        row.appendChild(duration)

        var boats = document.createElement("td")
        var boatsList = document.createElement("ul")
        if (element.stats != null) {
            var itemSwimmers = document.createElement("li")
            itemSwimmers.textContent = i18n.t("dashboard.item.swimmers") + ":" + element.stats.swimmers
            boatsList.appendChild(itemSwimmers)
            var itemSurfers = document.createElement("li")
            itemSurfers.textContent = i18n.t("dashboard.item.surfers") + ":" + element.stats.surfers
            boatsList.appendChild(itemSurfers)
            var itemFishing = document.createElement("li")
            itemFishing.textContent = i18n.t("dashboard.item.fishingboat") + ":" + element.stats.fish
            boatsList.appendChild(itemFishing)
            var itemFun = document.createElement("li")
            itemFun.textContent = i18n.t("dashboard.item.funboats") + ":" + element.stats.fun
            boatsList.appendChild(itemFun)
            var itemKite = document.createElement("li")
            itemKite.textContent = i18n.t("dashboard.item.kiteboats") + ":" + element.stats.kite
            boatsList.appendChild(itemKite)
        }
        boats.appendChild(boatsList)
        row.appendChild(boats)


        var products = document.createElement("td")
        var productsList = document.createElement("ul")

        if (element.products != null) {
            var splittedProducts = element.products.split(",")
            splittedProducts.forEach(dbItem => {
                var item = document.createElement("li")
                item.textContent = dbItem
                productsList.appendChild(item)
            })
        }
        products.appendChild(productsList)
        row.appendChild(products)

        document.getElementById("tableData").appendChild(row)
    });

}

function filter(cat, criteria) {
    var category = cat == "" ? "name" : cat
    if (jsonData.length > 0 && criteria != "") {
        var result = []
        if (category == "name")
            result = jsonData.filter(data => data.name.includes(criteria));
        if (category == "city")
            result = jsonData.filter(data => data.city.includes(criteria));
        if (category == "spot")
            result = jsonData.filter(data => data.spot.includes(criteria));

        displayData(result)
    } else {
        displayData(jsonData)
    }
}