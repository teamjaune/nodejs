﻿i18n.init({ lng: localStorage.getItem("language") != "undefined" ? localStorage.getItem("language") : "fr" }, function (err, t) {
	$("html").i18n();
	translatePlaceHolders();
});

var gameOfLife;

var leet = false;
var isUpsideDown = false;

window.addEventListener("DOMContentLoaded", function () {

	navigator.serviceWorker.register('../serviceWorker.js');

	if (localStorage.getItem("upsideDown") == "true") {
		upsideDown();
	}

	document.getElementById("frenchButton").addEventListener("click", function () {
		translate("fr");
		leet = false;
	});
	document.getElementById("englishButton").addEventListener("click", function () {
		translate("en");
		leet = false;
	});
	document.getElementById("upsideDownButton").addEventListener("click", function () {
		upsideDown();
	});

	window.onresize = function () {
		initGameOfLife();
	};

	initGameOfLife();
});

function upsideDown() {
	isUpsideDown = !isUpsideDown;
	let elements = getAllElementsWithAttribute("data-i18n");
	for (let i = 0; i < elements.length; i++) {
		let elem = elements[i];
		if (isUpsideDown)
			elem.classList.add("upside-down");
		else
			elem.classList.remove("upside-down");
	}
	localStorage.setItem("upsideDown", isUpsideDown ? "true" : "false");
}

function initGameOfLife() {
	var canvas = document.getElementById("game-of-life-canvas");

	var topBanner = document.getElementById("top-banner");
	if (topBanner != null && canvas != null) {
		canvas.width = topBanner.offsetWidth;
		canvas.height = topBanner.offsetHeight;

		gameOfLife = new GameOfLife();
		gameOfLife.init();

		canvas.addEventListener("contextmenu", function (e) {
			const rect = canvas.getBoundingClientRect()
			const x = e.clientX - rect.left;
			const y = e.clientY - rect.top;
			gameOfLife.interact(x, y, false);
			e.preventDefault();
		});

		canvas.addEventListener("click", function (e) {
			const rect = canvas.getBoundingClientRect()
			const x = e.clientX - rect.left;
			const y = e.clientY - rect.top;
			gameOfLife.interact(x, y, true);
			e.preventDefault();
		});

		window.setInterval(() => {
			gameOfLife.play();
		}, 75);
	}
}

var pattern = ['ArrowUp', 'ArrowUp', 'ArrowDown', 'ArrowDown', 'ArrowLeft', 'ArrowRight', 'ArrowLeft', 'ArrowRight', 'b', 'a'];
var current = 0;

var keyHandler = function (event) {
	if (pattern.indexOf(event.key) < 0 || event.key !== pattern[current]) {
		current = 0;
		return;
	}

	current++;

	if (pattern.length === current) {
		current = 0;
		if (leet) {
			translate(i18n.language);
		} else {
			translateToLeet();
		}
		leet = !leet;
	}
};

function translatePlaceHolders() {
	let placeholders = document.getElementsByClassName("placeholder");
	for (let i = 0; i < placeholders.length; i++) {
		placeholders[i].placeholder = i18n.t("form.placeholder");
	}
}

document.addEventListener('keydown', keyHandler, false);

function translate(language) {
	i18n.setLng(language, function (err, t) {
		$("html").i18n();
		document.title = i18n.t("app.name");
		translatePlaceHolders();
	});
	localStorage.setItem("language", language);
}


function translateToLeet() {
	let elements = getAllElementsWithAttribute("data-i18n");
	for (let i = 0; i < elements.length; i++) {
		let elem = elements[i];
		elem.textContent = toLeet(elem.textContent.toLowerCase());
	}
}

function toLeet(text) {
	let res = "";
	for (let i = 0; i < text.length; i++) {
		if (leetAlphabet[text[i]])
			res = res.concat(leetAlphabet[text[i]][0]);
	}
	return res;
}

function getAllElementsWithAttribute(attribute) {
	var matchingElements = [];
	var allElements = document.getElementsByTagName('*');
	for (var i = 0, n = allElements.length; i < n; i++) {
		if (allElements[i].getAttribute(attribute) !== null) {
			// Element exists with attribute. Add to array.
			matchingElements.push(allElements[i]);
		}
	}
	return matchingElements;
}

var leetAlphabet = {
	"a": [
		"4", "/\\", "@", "^"
	],
	"b": [
		"8", "6", "13", "|3"
	],
	"c": [
		"(", "&cent;", "<", "["
	],
	"d": [
		"[)", "|o", ")", "I>"
	],
	"e": [
		"3", "&", "&euro;", "&pound;"
	],
	"f": [
		"|=", "&fnof;", "|#", "ph"
	],
	"g": [
		"6", "&", "(_+", "9"
	],
	"h": [
		"#", "/-/", "[-]", "]-["
	],
	"i": [
		"1", "!", "|", "]["
	],
	"j": [
		"_|", "_/", "&iquest;", "</"
	],
	"k": [
		"X", "|<", "|{", "&#622;"
	],
	"l": [
		"1", "&pound;", "1_", "&#8467;"
	],
	"m": [
		"|v|", "[V]", "{V}", "|\\/|"
	],
	"n": [
		"^/", "|V", "|\\|", "/\\/"
	],
	"o": [
		"0", "()", "oh", "[]"
	],
	"p": [
		"|*", "|o", "|&ordm;", "|^(o)"
	],
	"q": [
		"(_,)", "()_", "0_", "&deg;|"
	],
	"r": [
		"2", "|?", "/2", "|^"
	],
	"s": [
		"5", "$", "z", "&sect;"
	],
	"t": [
		"7", "+", "-", "|-"
	],
	"u": [
		"(_)", "|_|", "v", "L|"
	],
	"v": [
		"\\/", "1/", "|/", "o|o"
	],
	"w": [
		"\\/\\/", "vv", "'//", "\\\\`"
	],
	"x": [
		"><", "&#1046;", "}{", "ecks"
	],
	"y": [
		"7", "j", "`/", "&Psi;"
	],
	"z": [
		"≥", "2", "=/=", "7_"
	]
}