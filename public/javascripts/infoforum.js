
async function getAutoSuggestionsFor(city) {
    return await fetch(`https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=vyoQ0od84E8vZXyf47ctA98952nOIAksV4zijEd3oa8&query=${city}`)
        .then(response => response.json())
        .then(data => { return data });
}

async function getPlaceNameAt(lng, lat) {
	let formatedPosition = lat + "%2C" + lng;
	return await fetch("https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=" + formatedPosition + "&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=" + "vyoQ0od84E8vZXyf47ctA98952nOIAksV4zijEd3oa8")
		.then(response => response.json())
		.then(data => { return data });
}

window.addEventListener("DOMContentLoaded", function () {
    document.getElementById("citySelect").addEventListener("input", function (event) {
        var listElement = document.getElementById("cityList")
        listElement.innerHTML = '';
        getAutoSuggestionsFor(event.target.value).then(values => {
            if (values.suggestions != undefined) {
                values.suggestions.forEach(element => {
                    var option = document.createElement("option");
                    option.value = element.label
                    listElement.appendChild(option)
                });
            }
        })
    });

	let wmname = localStorage.getItem("name");
	if (wmname != "undefined") {
		document.getElementById("wmname").value = wmname;
	}

    document.getElementById("sunscreenInput").style.visibility = "hidden"
    document.getElementById("sunscreenChk").addEventListener("click", function (event) {
        document.getElementById("sunscreenInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })
    document.getElementById("perfumeInput").style.visibility = "hidden"
    document.getElementById("perfumeChk").addEventListener("click", function (event) {
        document.getElementById("perfumeInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })
    document.getElementById("hydrationcreamInput").style.visibility = "hidden"
    document.getElementById("hydrationcreamChk").addEventListener("click", function (event) {
        document.getElementById("hydrationcreamInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })
    document.getElementById("makeupInput").style.visibility = "hidden"
    document.getElementById("makeupChk").addEventListener("click", function (event) {
        document.getElementById("makeupInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })
    document.getElementById("fuelInput").style.visibility = "hidden"
    document.getElementById("fuelChk").addEventListener("click", function (event) {
        document.getElementById("fuelInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })
    document.getElementById("cigarettesInput").style.visibility = "hidden"
    document.getElementById("cigarettesChk").addEventListener("click", function (event) {
        document.getElementById("cigarettesInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })
    document.getElementById("pesticideInput").style.visibility = "hidden"
    document.getElementById("pesticideChk").addEventListener("click", function (event) {
        document.getElementById("pesticideInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })
    document.getElementById("paintInput").style.visibility = "hidden"
    document.getElementById("paintChk").addEventListener("click", function (event) {
        document.getElementById("paintInput").style.visibility = event.target.checked ? "visible" : "hidden";
    })

	document.getElementById("currentPosition").addEventListener("click", function (e) {
		navigator.geolocation.getCurrentPosition(function (position) {
			getPlaceNameAt(position.coords.longitude, position.coords.latitude).then(name => {
				document.getElementById("citySelect").value = name.Response.View[0].Result[0].Location.Address.City;
			});
		});
		e.preventDefault();
	});

});


function validateForm() {
    document.getElementById("errors").innerText = "";
    //Session
    var name = document.getElementById("wmname").value
    if (name.length > 0 && name == "|v|02|*#3(_)5") {
        if (leet) {
            translate(i18n.language);
        } else {
            translateToLeet();
        }
        leet = !leet;
        return;
    }

    document.getElementById("errors").innerText = "SENDING ....."
    var spot = document.getElementById("spot").value
    var city = document.getElementById("citySelect").value
    var date = document.getElementById("date").value
    var startHour = document.getElementById("startHour").value
    var endHour = document.getElementById("endHour").value
    var duration = document.getElementById("duration").value
    //Statistics
    var swimmers = parseInt(document.getElementById("swimmersInput").value)
    var surfers = parseInt(document.getElementById("surfersInput").value)
    var vacationBoats = parseInt(document.getElementById("funBoatsCount").value)
    var fishingBoats = parseInt(document.getElementById("fishingBoatsCount").value)
    var kiteBoats = parseInt(document.getElementById("kiteBoatsCount").value)

    String.prototype.addProduct = function (char) {
        if (products == "")
            this.concat(value)
        else
            this.concat(",".concat(value))
    };
    var productslist = []

    var sunscreenChk = document.getElementById("sunscreenChk")
    var sunscreenInput = document.getElementById("sunscreenInput")
    if (sunscreenChk.checked) {
        var toAdd = "Sunscreen"
        if (sunscreenInput.value != "")
            toAdd += ("(" + sunscreenInput.value + ")")
        productslist.push(toAdd)
    }
    var perfumeChk = document.getElementById("perfumeChk")
    var perfumeInput = document.getElementById("perfumeInput")

    if (perfumeChk.checked) {
        var toAdd = "Perfume"
        if (perfumeInput.value != "")
            toAdd += ("(" + perfumeInput.value + ")")
        productslist.push(toAdd)
    }
    var hydrationcreamChk = document.getElementById("hydrationcreamChk")
    var hydrationcreamInput = document.getElementById("hydrationcreamInput")

    if (hydrationcreamChk.checked) {
        var toAdd = "Hydration Cream"
        if (hydrationcreamInput.value != "")
            toAdd += ("(" + hydrationcreamInput.value + ")")
        productslist.push(toAdd)
    }
    var makeupChk = document.getElementById("makeupChk")
    var makeupInput = document.getElementById("makeupInput")

    if (makeupChk.checked) {
        var toAdd = "Makeup"
        if (makeupInput.value != "")
            toAdd += ("(" + makeupInput.value + ")")
        productslist.push(toAdd)
    }
    var fuelChk = document.getElementById("fuelChk")
    var fuelInput = document.getElementById("fuelInput")

    if (fuelChk.checked) {
        var toAdd = "Fuel"
        if (fuelInput.value != "")
            toAdd += ("(" + fuelInput.value + ")")
        productslist.push(toAdd)
    }
    var cigarettesChk = document.getElementById("cigarettesChk")
    var cigarettesInput = document.getElementById("cigarettesInput")

    if (cigarettesChk.checked) {
        var toAdd = "Cigarettes"
        if (cigarettesInput.value != "")
            toAdd += ("(" + cigarettesInput.value + ")")
        productslist.push(toAdd)
    }
    var pesticideChk = document.getElementById("pesticideChk")
    var pesticideInput = document.getElementById("pesticideInput")

    if (pesticideChk.checked) {
        var toAdd = "Pesticide"
        if (pesticideInput.value != "")
            toAdd += ("(" + pesticideInput.value + ")")
        productslist.push(toAdd)
    }
    var paintChk = document.getElementById("paintChk")
    var paintInput = document.getElementById("paintInput")

    if (paintChk.checked) {
        var toAdd = "Paint"
        if (paintInput.value != "")
            toAdd += ("(" + paintInput.value + ")")
        productslist.push(toAdd)
    }
    var other = document.getElementById("other")
    if (other.value !== "") {
        productslist.push(other.value)
    }
    var products = productslist.join(",")

    console.log(products)
    fetch('/api/userform', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            data: {
                name: name,
                spot: spot,
                city: city,
                date: date,
                startHour: startHour,
                endHour: endHour,
                duration: duration,
                swimmers: swimmers,
                surfers: surfers,
                fishingBoats: fishingBoats,
                vacationBoats: vacationBoats,
                kiteBoats: kiteBoats,
                products: products
            }
        })
    }).then(async value => {
        var val = await value.json()
        var err = ""
        if (val.code != null) {
            switch (val.code) {
                case 100:
                    err = i18n.t("form.errors.noErr");
                    break;
                case 101:
                    err = i18n.t("form.errors.errName");
                    break;
                case 102:
                    err = i18n.t("form.errors.errSpot");
                    break;
                case 103:
                    err = i18n.t("form.errors.errCity");
                    break;
                case 104:
                    err = i18n.t("form.errors.errDate");
                    break;
                case 105:
                    err = i18n.t("form.errors.errStart");
                    break;
                case 106:
                    err = i18n.t("form.errors.errEnd");
                    break;
                case 107:
                    err = i18n.t("form.errors.errDuration");
                    break;
                default:
                    break;
            }
        } else {
            document.getElementById("errors").innerText = "We are having issues with sending the data to the server";
            document.getElementById("errors").style.color = "red";
        }

        document.getElementById("errors").innerText = err;
        if (val.code != 100)
            document.getElementById("errors").style.color = "red";
		else {
			localStorage.setItem("name", name);
            document.getElementById("errors").style.color = "green";
        }
    });


}
