//Constants of the game of life
const CELL_SIZE = 8;
const ALIVE_COLOR = "#1f4c99";
const DEAD_COLOR = "#d9d9d9"
const PERCENTAGE_PREFILL_ALIVE = 0.6;

var canvas;
var ctx;

//Class that handles a game of life
class GameOfLife {

	constructor() {
		canvas = document.getElementById("game-of-life-canvas");
		ctx = canvas.getContext("2d");

		this.columns = Math.floor(canvas.width / CELL_SIZE);
		this.rows = Math.floor(canvas.height / CELL_SIZE);

		this.aliveCells = [];
		this.tmpCells = [];
		this.cellsToBeAdded = [];

		//Randomly prefill the grid
		this.prefill = () => {
			for (let i = 0; i < this.rows; i++)
				for (let j = 0; j < this.columns; j++)
					this.aliveCells[i][j] = (Math.random() > PERCENTAGE_PREFILL_ALIVE) ? 1 : 0;
		};

		//Render the new state of the game
		this.render = () => {
			for (let i = 0; i < this.rows; i++) {
				for (let j = 0; j < this.columns; j++) {
					let color;
					if (this.aliveCells[i][j] == 1)
						color = ALIVE_COLOR;
					else
						color = DEAD_COLOR;
					ctx.fillStyle = color;
					ctx.fillRect(j * CELL_SIZE, i * CELL_SIZE, CELL_SIZE, CELL_SIZE);
				}
			}
		};

		this.init = () => {
			this.initCells();
			this.prefill();
			this.render();
		};

		//Plays a new round of the game
		this.play = () => {
			this.update();
			this.render();
		};

		//Update the state of the game
		this.update = () => {

			for (let i = 0; i < this.rows; i++) {
				for (let j = 0; j < this.columns; j++) {
					let new_state = this.updateCellValue(i, j);
					this.tmpCells[i][j] = new_state;
				}
			}

			for (let i = 0; i < this.rows; i++) {
				for (let j = 0; j < this.columns; j++) {
					if (this.hasToBeAdded(j, i)) {
						this.tmpCells[i][j] = 1;
					}
				}
			}

			for (let i = 0; i < this.rows; i++) {
				this.aliveCells[i] = [];
				for (let j = 0; j < this.columns; j++)
					this.aliveCells[i][j] = this.tmpCells[i][j];
			}

			this.cellsToBeAdded = [];
		};

		//Used to check if a cell position has been clicked by the user
		this.hasToBeAdded = (row, col) => {
			for (let i = 0; i < this.cellsToBeAdded.length; i++) {
				let cell = this.cellsToBeAdded[i];
				if (cell.row == row && cell.col == col)
					return true;
			}
			return false;
		};

		//First initialization of the cells
		this.initCells = () => {

			for (let i = 0; i < this.rows; i++) {
				this.aliveCells[i] = [];
				for (let j = 0; j < this.columns; j++)
					this.aliveCells[i][j] = 0;
			}

			for (let i = 0; i < this.rows; i++) {
				this.tmpCells[i] = [];
				for (let j = 0; j < this.columns; j++)
					this.tmpCells[i][j] = 0;
			}
		};

		//Checks if a cell is alive
		this.isAlive = (row, col) => {
			if (row >= this.rows || row < 0 || col < 0 || col >= this.columns)
				return 0;

			return this.aliveCells[row][col];
		};

		//Counts the neighbours of a cell
		this.countNeighbours = (row, col) => {
			let neighbours = 0;
			neighbours += this.isAlive(row - 1, col);
			neighbours += this.isAlive(row - 1, col - 1);
			neighbours += this.isAlive(row - 1, col + 1);
			neighbours += this.isAlive(row, col - 1);
			neighbours += this.isAlive(row, col + 1);
			neighbours += this.isAlive(row + 1, col);
			neighbours += this.isAlive(row + 1, col - 1);
			neighbours += this.isAlive(row + 1, col + 1);
			return neighbours;
		};

		//Updates a cell using the game of life's rules
		this.updateCellValue = (row, col) => {
			const neighbours = this.countNeighbours(row, col);

			if ((this.isAlive(row, col) && neighbours == 2) || neighbours == 3) {
				return 1;
			}
			return 0;
		};

		//Called when the user interacts with the canvas
		this.interact = (row, col, glider) => {
			row = Math.round(row / CELL_SIZE);
			col = Math.round(col / CELL_SIZE);

			this.cellsToBeAdded.push({ "row": row, "col": col });	
			if (glider) {
				this.cellsToBeAdded.push({ "row": row + 1, "col": col + 1 });
				this.cellsToBeAdded.push({ "row": row + 2, "col": col + 1 });
				this.cellsToBeAdded.push({ "row": row + 2, "col": col});
				this.cellsToBeAdded.push({ "row": row + 2, "col": col - 1 });
			}
		};

	}
}