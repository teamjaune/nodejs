const URL = "https://api.worldweatheronline.com/premium/v1/marine.ashx";
const API_KEY = "7236076df173427b89a161958200312";

function getLocalMarineWeather(long, lat, hour) {
	var url = URL + "?q=" + lat + "," + long + " &tp=1&key=" + API_KEY;

	var xmlhttp = new XMLHttpRequest();

	xmlhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			let xml = this.responseXML;
			fillWeather(xml.children[0].children[1].children[6 + hour]);
		}
	};
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}

async function getAutoSuggestionsFor(city) {
	return await fetch(`https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json?apiKey=vyoQ0od84E8vZXyf47ctA98952nOIAksV4zijEd3oa8&query=${city}`)
		.then(response => response.json())
		.then(data => { return data });
}
async function getCoordinatesFor(city) {
	return await fetch("https://geocode.search.hereapi.com/v1/geocode?apiKey=vyoQ0od84E8vZXyf47ctA98952nOIAksV4zijEd3oa8&q=" + city)
		.then(response => response.json())
		.then(data => { return data });
}
async function getPlaceNameAt(lng, lat) {
	let formatedPosition = lat + "%2C" + lng;
	return await fetch("https://reverse.geocoder.ls.hereapi.com/6.2/reversegeocode.json?prox=" + formatedPosition + "&mode=retrieveAddresses&maxresults=1&gen=9&apiKey=" + "vyoQ0od84E8vZXyf47ctA98952nOIAksV4zijEd3oa8")
		.then(response => response.json())
		.then(data => { return data });
}

function fillWeather(hourly) {

	let tempC = hourly.children[1].textContent;
	let windspeedKmph = hourly.children[4].textContent;
	let winddir16Point = hourly.children[6].textContent;
	let weatherIconUrl = hourly.children[8].textContent;
	let precipMM = hourly.children[10].textContent;
	let visibility = hourly.children[13].textContent;
	let swellHeight_m = hourly.children[29].textContent;
	let swellDir16Point = hourly.children[32].textContent;
	let waterTemp_C = hourly.children[34].textContent;

	document.getElementById("tempC").innerText = tempC;
	document.getElementById("windspeedKmph").innerText = windspeedKmph;
	document.getElementById("winddir16Point").innerText = winddir16Point;
	document.getElementById("weatherIconUrl").innerHTML = "<img style='border-radius:15%;' src=" + weatherIconUrl + ">";
	document.getElementById("precipMM").innerText = precipMM;
	document.getElementById("visibility").innerText = visibility;
	document.getElementById("swellHeight_m").innerText = swellHeight_m;
	document.getElementById("swellDir16Point").innerText = swellDir16Point;
	document.getElementById("waterTemp_C").innerText = waterTemp_C;
}

window.addEventListener("DOMContentLoaded", function () {

	let date = new Date();
	document.getElementById("hour").value = (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + date.getMinutes();

	document.getElementById("citySelect").addEventListener("input", function (event) {
		var listElement = document.getElementById("cityList")
		listElement.innerHTML = '';
		getAutoSuggestionsFor(event.target.value).then(values => {
			if (values.suggestions != undefined) {
				values.suggestions.forEach(element => {
					var option = document.createElement("option");
					option.value = element.label
					listElement.appendChild(option)
				});
			}
		});
	});

	document.getElementById("currentPosition").addEventListener("click", function (e) {

		navigator.geolocation.getCurrentPosition(function (position) {
			getPlaceNameAt(position.coords.longitude, position.coords.latitude).then(name => {
				document.getElementById("citySelect").value = name.Response.View[0].Result[0].Location.Address.City;
			});
		});
		e.preventDefault();
	});

	document.getElementById("searchForWeatherInfo").addEventListener("click", function (e) {
		let city = document.getElementById("citySelect").value;
		if (city == "") {
			e.preventDefault();
			document.getElementById("errors").innerText = i18n.t("weatherConditions.err");
			document.getElementById("errors").style.color = "red";
			return;
		}

		document.getElementById("errors").innerText = "";

		let coords = getCoordinatesFor(city);
		coords.then(coords => {
			let position = coords.items[0].position;
			let lat = position.lat;
			let lng = position.lng;

			let hour = parseInt(document.getElementById("hour").value.substring(0, 2), 10);

			getLocalMarineWeather(lng, lat, hour);
		});
		e.preventDefault();
	});

});