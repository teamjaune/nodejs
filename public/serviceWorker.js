"use strict";

// variable definitions 
var CACHE_NAME = 'teamJaune-v1';

var contentToCache = [
    './',
    './manifest.webmanifest',
    './images/translation/en-US.png',
    './images/translation/fr-FR.png',
    './javascripts/jquery.js',
    './javascripts/i18next-1.11.2/i18next-1.11.2.js',
    './locales/en/translation.json',
    './locales/fr/translation.json'
];

// Service worker installation
self.addEventListener('install', function (e) {
    console.log('[Service Worker] Install');
    e.waitUntil(caches.open(CACHE_NAME).then(function (cache) {
        console.log('[Service Worker] Caching application content & data');
        return cache.addAll(contentToCache);
    }));
});

self.addEventListener('fetch', (e) => {


    e.respondWith(fetch(e.request)
        .then((response) => {
            return caches.open(CACHE_NAME).then((cache) => {
                console.log('[Service Worker] Fetching from network and caching resource: ' + e.request.url);
                cache.put(e.request, response.clone());
                return response;
            });
        })
        .catch(function () {
            return caches.match(e.request).then((r) => {
                console.log('[Service Worker] Looking for resource in cache: ' + e.request.url);
                return r; // || new Response(JSON.stringify({ error: 1 }), { headers: { 'Content-Type': 'application/json' } }); 
            })
        })
    );

});

self.addEventListener('activate', (e) => {
    e.waitUntil(
        // Cleaning previous caches
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
                if (CACHE_NAME.indexOf(key) === -1) {
                    console.log("[Service Worker] Cleaning old cache");
                    return caches.delete(key);
                }
            }));
        })
    );
});