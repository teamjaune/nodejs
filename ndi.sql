-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Ven 04 Décembre 2020 à 05:16
-- Version du serveur :  5.7.32-0ubuntu0.18.04.1
-- Version de PHP :  7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `ndi`
--

-- --------------------------------------------------------

--
-- Structure de la table `statistics`
--

CREATE TABLE `statistics` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `spot` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `start` time(6) NOT NULL,
  `end` time(6) NOT NULL,
  `duration` time(6) NOT NULL,
  `swimmers` int(11) NOT NULL,
  `surfers` int(11) NOT NULL,
  `fishingboats` int(11) NOT NULL,
  `funboats` int(11) NOT NULL,
  `kiteboats` int(11) NOT NULL,
  `products` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Contenu de la table `statistics`
--

INSERT INTO `statistics` (`id`, `name`, `spot`, `city`, `date`, `start`, `end`, `duration`, `swimmers`, `surfers`, `fishingboats`, `funboats`, `kiteboats`, `products`) VALUES
(6, 'Villemin Victor', 'Lacanau-Océan', 'France, Lacanau, Lacanau Océan', '2020-12-03', '08:00:00.000000', '16:09:00.000000', '04:16:00.000000', 12, 0, 0, 0, 4, 'Sunscreen'),
(7, 'Yousif', 'Rue de la gare', 'Serre-les-Sapins', '2020-12-04', '06:09:00.000000', '08:00:00.000000', '01:04:00.000000', 2, 1, 0, 5, 1, 'Sunscreen,Fuel(SP95)'),
(8, 'Romain', '9 rue Combe au Roucheret', 'France, Serre-les-Sapins', '2020-12-04', '06:14:00.000000', '06:17:00.000000', '00:03:00.000000', 6, 1, 3, 2, 1, 'Perfume,Fuel(SP98)');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `statistics`
--
ALTER TABLE `statistics`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `statistics`
--
ALTER TABLE `statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
