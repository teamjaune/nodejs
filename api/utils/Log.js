var callerId = require('caller-id');
const fs = require('fs');
const path = require('path');
const winston = require('winston');
const { combine, timestamp, label, printf } = winston.format;

const customLevels = {
    levels: {
        ERROR: 0,
        WARN: 1,
        SUCC: 1,
        INFO: 3,
        HTTP: 4,
        CLIENT: 5,
        DB: 6,
        ACCESS: 7,
        EMAIL: 8,

    },
    colors: {
        ERROR: 'red',
        WARN: 'yellow',
        SUCC: 'green',
        INFO: 'white',
        HTTP: 'blue',
        CLIENT: 'red',
        DB: 'cyan',
        ACCESS: 'green',
        EMAIL: 'green'

    }
}

winston.addColors(customLevels.colors);

const myFormat = printf(({ level, message, source, timestamp }) => {
    return `${timestamp} [${source}] [${level}]: ${message}`;
});

const options = {
    level: 'ACCESS',
    levels: customLevels.levels,
    format: combine(
        winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
        winston.format.json()
    ),
    transports: [
        new winston.transports.File({ filename: path.resolve(__dirname + '/../logs/server.log'), timestamp: true }),

        new winston.transports.Console(
            {
                format: winston.format.combine(
                    winston.format.colorize(),
                    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
                    myFormat
                )
            })

    ]
}

const Infologger = winston.createLogger(options);


var log = {
    info: function (message) {
        var caller = callerId.getData();
        Infologger.log('INFO', message, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    warning: function (message) {
        var caller = callerId.getData();
        Infologger.log('WARN', message, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    error: function (message) {
        var caller = callerId.getData();
        Infologger.log('ERROR', message, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    success: function (message) {
        var caller = callerId.getData();
        Infologger.log('SUCC', message, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    http: function (message) {
        var caller = callerId.getData();
        Infologger.log('HTTP', message, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    database: function (message) {
        var caller = callerId.getData();
        Infologger.log('DB', message, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    access: function (ip, user) {
        var caller = callerId.getData();
        if (user)
            Infologger.log('ACCESS', `User ${user} from ${ip}`, { source: (caller.filePath + ":" + caller.lineNumber) });
        else
            Infologger.log('ACCESS', `${ip}`, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    email: function (emailInfo) {
        var caller = callerId.getData();
        Infologger.log('EMAIL', emailInfo, { source: (caller.filePath + ":" + caller.lineNumber) });
    },
    client: function (message) {
        console.log(message)
        Infologger.log('CLIENT', message, { source: "client " });
    },

    getLogs: function (req, res) {
        // -----------------------------------------------------------------------
        // authentication middleware

        const auth = { login: 'Admin', password: 'Admin' } // change this

        // parse login and password from headers
        const b64auth = (req.headers.authorization || '').split(' ')[1] || ''
        const [login, password] = Buffer.from(b64auth, 'base64').toString().split(':')

        // Verify login and password are set and correct
        if (!login && login != auth.login) {


            // Access denied...
            res.set('WWW-Authenticate', 'Basic realm="401"') // change this
            res.status(401).send('Authentication required.') // custom message
            // Access granted...
        }
        // -----------------------------------------------------------------------

        console.log(req.userID)
        //module.exports.info("Processing log request")
        module.exports.access(req.connection.remoteAddress, req.userID)
        if (req.elevatedPrivileges) {
            const options = {
                from: new Date() - (24 * 60 * 60 * 1000),
                until: new Date(),
                limit: 5000,
                start: 0,
                order: 'desc',
            };
            //
            // Find items logged between today and yesterday.
            //
            Infologger.query(options, function (err, results) {
                if (err) {
                    console.log(err);
                    return
                }
                res.send(results)
                // console.log(results);
            });
        }
        //module.exports.info("Finished processing log request")

    }
};

module.exports = log

