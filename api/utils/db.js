var mysql = require('mysql');
var pool = mysql.createPool({
    connectionLimit: 10,
    host: process.env.NODE_SQL_DATABASE_ADDRESS,
    user: process.env.NODE_SQL_DATABASE_USER,
    password: process.env.NODE_SQL_DATABASE_PASSWORD,
    database: process.env.NODE_SQL_DATABASE_NAME
});
module.exports = pool;

