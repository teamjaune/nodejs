'use strict';
module.exports = function (app) {
    var logger = require("./utils/Log")
    //------------- Initilize database -------------//
    const db = require('./utils/db.js');

    app.route('/api/userform')
        .post((req, res) => {
            logger.access(req.connection.remoteAddress, 0)
            var body = req.body.data

            //Session
            var watermanName = body["name"]
            var spot = body["spot"]
            var city = body["city"]
            var date = body["date"]
            var startHour = body["startHour"]
            var endHour = body["endHour"]
            var duration = body["duration"]
            res.setHeader('Content-Type', 'application/json');
            if (watermanName == null || watermanName === "") {
                res.status(400).json({ code: 101 });
                return;
            }
            if (spot == null || spot === "") {
                res.status(400).json({ code: 102 });
                return;
            }
            if (city == null || city === "") {
                res.status(400).json({ code: 103 });
                return;
            }
            if (date == null || date === "") {
                res.status(400).json({ code: 104 });
                return;
            }
            if (startHour == null || startHour === "") {
                res.status(400).json({ code: 105 });
                return;
            }
            if (endHour == null || endHour === "") {
                res.status(400).json({ code: 106 });
                return;
            }
            if (duration == null || duration === "") {
                res.status(400).json({ code: 107 });
                return;
            }
            //Statistics
            var swimmers = body["swimmers"] || 0
            var surfers = body["surfers"] || 0
            var fishingBoats = body["fishingBoatsCount"] || 0
            var vacationBoats = body["vacationBoats"] || 0
            var fishingBoats = body["fishingBoats"] || 0
            var kiteBoats = body["kiteBoats"] || 0
            var productBoats = body["products"] || ""

            var query = db.query(`INSERT INTO statistics (name, spot, city, date, start, end, duration, swimmers, surfers, fishingboats, funboats, kiteboats, products)  VALUES ('${watermanName}', '${spot}', '${city}', '${date}', '${startHour}', '${endHour}', '${duration}', '${swimmers}', '${surfers}', '${fishingBoats}', '${vacationBoats}', '${kiteBoats}', '${productBoats}')`, function (error, results, fields) {
                if (error) throw error;
                // Neat!
            });

            res.set('Cache-Control', 'no-store')
            res.status(200).json({ code: 100 });
        })

    app.route('/api/dashboard/statistics')
        .get(async (req, res) => {
            logger.access(req.connection.remoteAddress, 0)
            var results = []
            await new Promise(function (resolve, reject) {

                db.query(`select * from statistics`, function (error, rows, fields) {
                    if (error) throw error;
                    for (var i = 0; i < rows.length; i++) {

                        var data = {}
                        data["id"] = rows[i].id;
                        data["name"] = rows[i].name;
                        data["city"] = rows[i].city;
                        data["spot"] = rows[i].spot;
                        data["date"] = rows[i].date;
                        data["start"] = rows[i].start;
                        data["end"] = rows[i].end;
                        data["duration"] = rows[i].duration;
                        var stats = { swimmers: rows[i].swimmers, surfers: rows[i].surfers, fish: rows[i].fishingboats, fun: rows[i].funboats, kite: rows[i].kiteboats }
                        data["stats"] = stats
                        data["products"] = rows[i].products;
                        results.push(data)

                    }
                    resolve(true);

                    // Neat!
                });
            })
            res.contentType('application/json');
            res.send(JSON.stringify(results));
        })
}