/************************************************************/
/* Import external libs                                     */
/************************************************************/
var express = require('express');
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser');
var logger = require("./api/utils/Log")
var fs = require("fs");
var https = require("https");

/************************************************************/
/* Import a parser for environement variables               */
/************************************************************/
require('dotenv').config();
/************************************************************
save as .env file in the root directory                                  

NODE_PORT_HTTPS=
NODE_PORT_HTTP=
NODE_SQL_DATABASE_ADDRESS=
NODE_SQL_DATABASE_USER=
NODE_SQL_DATABASE_PASSWORD=
NODE_SQL_DATABASE_NAME=  
NODE_CERT_KEY_PATH=localhost.key
NODE_CERT_PATH=localhost.crt  
************************************************************/

//------------- Initilize web server and api routes -------------//
const app = express();
app.use(cookieParser());
app.use(express.json());

logger.info("Initilizing Webserver routes")
//setup paths
var routes = require('./api/Router'); //importing route
routes(app); //register the route
app.use(express.static('public'))// serve static files
app.use(function (req, res, next) {
  res.redirect("/404.html")
});
logger.info("Web Server is Starting")
//start the server
var httpPort = process.env.NODE_PORT_HTTP || 80;
var httpsPort = process.env.NODE_PORT_HTTPS || 443;
const options = {
  key: fs.readFileSync(process.env.NODE_CERT_KEY_PATH || "localhost.key"),
  cert: fs.readFileSync(process.env.NODE_CERT_PATH || "localhost.crt")
};

// set up plain http server
var http = express();

// set up a route to redirect http to https
http.get('*', function (req, res) {
  res.redirect('https://' + req.headers.host + req.url);

  // Or, if you don't want to automatically detect the domain name from the request header, you can hard code it:
  // res.redirect('https://example.com' + req.url);
})

// have it listen on 8080
http.listen(httpPort);


https.createServer(options, app).listen(httpsPort);
logger.success(`Web Server is Up on port ${httpPort}(http) ${httpsPort}(https)`)

